import React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import DashboardComponent from './features/Dashboard/DashboardComponent';
import { AddUserComponent } from './features/AddUser/AddUserComponent';
import { EditUserComponent } from './features/EditUser/EditUserComponent';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<DashboardComponent />} />
      </Routes>
      <Routes>
        <Route path="/addUser" element={<AddUserComponent />} />
      </Routes>
      <Routes>
        <Route path="/editUser/:id" element={<EditUserComponent />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
