import React, { useState } from 'react';
import { postData } from '../../store/dashboard-slice';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {
  Form,
  Button,
  Container,
  Card,
  Col,
  Row,
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
/**
 * @author
 * @function AddUserComponent
 **/
const initialData = {
  name: '',
  email: '',
};
export const AddUserComponent = (props) => {
  const [data, setValues] = useState(initialData);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleInputs = (e) => {
    const { id, value } = e.target;
    setValues({
      ...data,
      [id]: value,
    });
  };
  const payload = {
    name: data.name,
    email: data.email,
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(postData(payload));
    setTimeout(function () {
      navigate('/');
    }, 1500);
  };
  const homePath = () => {
    navigate('/');
  };
  return (
    <Container>
      <h1>Dashboard</h1>
      <Card>
        <Container>
          <Card.Body>
            <Card.Title className="d-flex justify-content-between">
              Form
            </Card.Title>
            <Card>
              <Container>
                <Form className="pt-3" onSubmit={handleSubmit}>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    // controlId="formPlaintextEmail"
                  >
                    <Form.Label column sm="2">
                      Name
                    </Form.Label>
                    <Col sm="10">
                      <Form.Control
                        type="text"
                        placeholder="Name"
                        id="name"
                        onChange={handleInputs}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="formPlaintextPassword"
                  >
                    <Form.Label column sm="2">
                      Email
                    </Form.Label>
                    <Col sm="10">
                      <Form.Control
                        type="email"
                        placeholder="Email"
                        id="email"
                        onChange={handleInputs}
                      />
                    </Col>
                  </Form.Group>
                </Form>
              </Container>
            </Card>
            <Button
              className="mt-3 me-2"
              variant="outline-danger"
              onClick={homePath}
            >
              Cancel
            </Button>
            <Button
              className="mt-3"
              variant="success"
              type="submit"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </Card.Body>
        </Container>
      </Card>
    </Container>
  );
};
