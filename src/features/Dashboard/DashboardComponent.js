import React from 'react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Button,
  Container,
  Table,
  Card,
  Modal,
} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { fetchData } from '../../store/dashboard-slice';
import { fetchLocalData } from '../../store/dashboard-slice';
import { deleteData } from '../../store/dashboard-slice';
import { useNavigate } from 'react-router-dom';

/**
 * @author
 * @function DashboardComponent
 **/

const DashboardComponent = (props) => {
  const dispatch = useDispatch();
  const { data } = useSelector((state) => state.dashboard);
  const { localData } = useSelector((state) => state.dashboard);
  const navigate = useNavigate();
  const [show, setShow] = useState(false);

  const handleClose = (e) => {
    e.preventDefault();
    dispatch(deleteData(e.target.id));
    dispatch(fetchLocalData());
    setShow(false);
  };
  const handleShow = (e) => {
    setShow(true);
  };
  useEffect(() => {
    dispatch(fetchData());
  }, [dispatch]);
  useEffect(() => {
    dispatch(fetchLocalData());
  }, [dispatch]);

  const addUserPath = () => {
    navigate('/addUser');
  };
  const EditUserPath = (e) => {
    let id = e.target.id;
    navigate(`/editUser/${id}`);
  };

  return (
    <Container>
      <h1>Dashboard</h1>
      <Card>
        <Container>
          <Card.Body>
            <Card.Title className="d-flex justify-content-between">
              User List
            </Card.Title>
            <Table striped hover bordered size="sm" responsive>
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>City</th>
                </tr>
              </thead>
              <tbody>
                {Object.values(data).map((dat, index) => (
                  <tr key={index}>
                    <td>{dat.id}</td>
                    <td>{dat.name}</td>
                    <td>{dat.username}</td>
                    <td>{dat.email}</td>
                    <td>{dat.address.city}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Container>
      </Card>
      <br />
      <br />
      <br />
      <Card>
        <Container>
          <Card.Body>
            <Card.Title className="d-flex justify-content-between">
              User List Local Data
              <Button variant="primary" onClick={addUserPath}>
                Add New
              </Button>
            </Card.Title>
            <Table striped hover bordered size="sm" responsive>
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                {Object.values(localData).map((dat, index) => (
                  <tr key={index}>
                    <td>{dat.id}</td>
                    <td>{dat.name}</td>
                    <td>{dat.email}</td>
                    <td>
                      <Button
                        variant="warning"
                        id={dat.id}
                        onClick={EditUserPath}
                      >
                        Edit
                      </Button>
                    </td>
                    <td>
                      <Button variant="danger" id={dat.id} onClick={handleShow}>
                        Delete
                      </Button>
                      <Modal show={show} onHide={handleClose} id={dat.id}>
                        <Modal.Header closeButton>
                          <Modal.Title>Delete User</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          Are you sure do you want to delete?
                        </Modal.Body>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={handleClose}>
                            Close
                          </Button>
                          <Button
                            variant="danger"
                            id={dat.id}
                            onClick={handleClose}
                          >
                            Delete
                          </Button>
                        </Modal.Footer>
                      </Modal>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Card.Body>
        </Container>
      </Card>
    </Container>
  );
};
export default DashboardComponent;
