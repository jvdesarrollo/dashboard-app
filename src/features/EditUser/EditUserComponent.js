import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { updateData } from '../../store/dashboard-slice';
import { Form, Button, Container, Card, Col, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
/**
 * @author
 * @function EditUserComponent
 **/

export const EditUserComponent = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { localData } = useSelector((state) => state.dashboard);

  const { id } = useParams();
  const userData = localData.filter((user) => user.id === +id);
  const [data, setValues] = useState(userData[0]);

  const homePath = () => {
    navigate('/');
  };
  const handleInputs = (e) => {
    const { id, value } = e.target;
    setValues({
      ...data,
      [id]: value,
    });
  };
  const payload = {
    ...userData[0],
    name: data.name,
    email: data.email,
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(updateData(payload, userData[0].id));
    setTimeout(function () {
      navigate('/');
    }, 1500);
  };
  return (
    <Container>
      <h1>Dashboard</h1>
      <Card>
        <Container>
          <Card.Body>
            <Card.Title className="d-flex justify-content-between">
              Form
            </Card.Title>
            <Card>
              <Container>
                <Form className="pt-3" id={id}>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    // controlId="formPlaintextEmail"
                  >
                    <Form.Label column sm="2">
                      Name
                    </Form.Label>
                    <Col sm="10">
                      <Form.Control
                        type="text"
                        placeholder="Name"
                        id="name"
                        onChange={handleInputs}
                        defaultValue={userData[0] && userData[0].name}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group
                    as={Row}
                    className="mb-3"
                    controlId="formPlaintextPassword"
                  >
                    <Form.Label column sm="2">
                      Email
                    </Form.Label>
                    <Col sm="10">
                      <Form.Control
                        type="email"
                        placeholder="Email"
                        id="email"
                        onChange={handleInputs}
                        defaultValue={userData[0] && userData[0].email}
                      />
                    </Col>
                  </Form.Group>
                </Form>
              </Container>
            </Card>
            <Button
              className="mt-3 me-2"
              variant="outline-danger"
              onClick={homePath}
            >
              Cancel
            </Button>
            <Button
              className="mt-3"
              variant="success"
              type="submit"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </Card.Body>
        </Container>
      </Card>
    </Container>
  );
};
