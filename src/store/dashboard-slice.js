import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
const dashboardSlice = createSlice({
  name: 'dashboard',
  initialState: {
    data: [],
    localData: [],
  },
  reducers: {
    getDashboardData(state, action) {
      state.data = action.payload;
    },
    getDashboardLocalData(state, action) {
      state.localData = action.payload;
    },
    addUser(state, action) {
      state.data = action.payload;
    },
  },
});
export const { getDashboardData, getDashboardLocalData, addUser } =
  dashboardSlice.actions;
export default dashboardSlice;

export const fetchData = () => (dispatch) => {
  axios
    .get(
      'https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb/data'
    )
    .then((response) => {
      // console.log('response', response.data);
      dispatch(getDashboardData(response.data));
    })
    .catch((err) => console.log(err));
};
export const fetchLocalData = () => (dispatch) => {
  axios
    .get('http://localhost:8000/data')
    .then((response) => {
      dispatch(getDashboardLocalData(response.data));
    })
    .catch((err) => console.log(err));
};
export const postData = (payload) => (dispatch) => {
  axios
    .post('http://localhost:8000/data', {
      name: payload.name,
      email: payload.email,
    })
    .then((response) => {
      console.log('response', response.data);
    })
    .catch((err) => console.log(err));
};
export const deleteData = (id) => () => {
  axios
    .delete(`http://localhost:8000/data/${id}`)
    .then((response) => {
      console.log('response', response.data);
    })
    .catch((err) => console.log(err));
};

export const updateData = (payload, id) => () => {
  axios
    .put(`http://localhost:8000/data/${id}`, {
      name: payload.name,
      email: payload.email,
    })
    .then((response) => {
      console.log('response', response.data);
    })
    .catch((err) => console.log(err));;
};
